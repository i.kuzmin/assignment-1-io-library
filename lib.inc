section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

.loop:
    cmp byte [rdi + rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rdx, rdx
    mov rax, rdi
    mov rbx, 10
    mov r12, rsp
    dec rsp
    mov [rsp], byte 0
    
.loop_div:
    xor rdx, rdx
    div rbx
    add rdx, '0'
    dec rsp
    mov [rsp], dl
    test rax, rax
    jnz .loop_div

    mov rdi, rsp
    call print_string
    mov rsp, r12
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .print
    mov rdx, rdi
    neg rdx
    mov rdi, '-'
    push rdx
    call print_char
    pop rdx
    mov rdi, rdx

.print:
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    
.compare:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx

.read:
    mov al, [rdi + rcx]
    mov dl, [rsi + rcx]
    cmp al, dl
    jne .failure
    cmp al, 0
    je .end
    inc rcx
    jmp .read

.failure:
    xor rax, rax
    ret

.end:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall

    cmp rax, 1
    je .success
    xor rax, rax
    inc rsp
    ret

.success:
    xor rax, rax
    mov al, byte[rsp]
    inc rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
    push rdi
    push rsi
    push rdx

.start_strip:
    call read_char
    call .whitespace_check
    jz .start_strip

.read:
    pop rdx
    pop rsi
    pop rdi
    call .whitespace_check
    test rax, rax
    jz .end

    mov [rdi + rdx], al
    inc rdx

    dec rsi
    jz .end
    push rdi
    push rsi
    push rdx
    call read_char
    jmp .read

.end:
    test rsi, rsi
    jnz .success
    xor rax, rax
    ret

.success:
    mov byte[rdi + rdx], 0
    mov rax, rdi
    ret

; Without convention because local for read_word
; Return 0 if space symbol or symbol otherwise
.whitespace_check:
    cmp rax, ' '
    je .whitespace
    cmp rax, `\t`
    je .whitespace
    cmp rax, `\n`
    je .whitespace
    ret

.whitespace:
    xor rax, rax
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
.read:
    mov cl, [rdi + rdx]

    cmp rcx, '0'
    jl .end
    cmp rcx, '9'
    jg .end

    sub rcx, '0'
    imul rax, 10
    add rax, rcx
    inc rdx

    jmp .read

.end:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, [rdi]
    cmp rax, '-'
    jne parse_uint

.neg:
    inc rdi
    call parse_uint
    neg rax
    inc rdx

    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jge .failure
    xor rax, rax
    xor rdx, rdx

.loop:
    mov dl, [rdi + rax]
    mov [rsi + rax], dl
    test dl, dl
    je .end
    inc rax
    jmp .loop

.end:
    ret

.failure:
    xor rax, rax
    ret

    
